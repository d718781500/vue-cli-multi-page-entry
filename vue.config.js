console.log(process.argv);
let glob = require('glob');
let productName = process.argv[3]//获取package.json中配置的指令名字
console.log(glob.sync('./src/*/*.js'))
function createEntry() {
    let entries = {}
    if (process.env.NODE_ENV === "production") {
        entries = {
            index: {
                // page的入口
                entry: `src/${productName}/main.js`,
                // 模板来源
                template: `src/${productName}/public/index.html`,
                // 在 dist/index.html 的输出
                filename: `index.html`,
                title: `这是${projectName}端`,
                chunks: ['chunk-vendors', 'chunk-common', 'index']
            }
        }
    } else {

        let paths = glob.sync('./src/*/*.js');

        paths.forEach(path => {
            let pathStr = path.split("/") //[ '.', 'src', 'pc', 'main.js' ]
            //字面量命名
            entries[[pathStr[2]]] = {
                // page的入口
                entry: `src/${pathStr[2]}/${pathStr[3]}`,
                // 模板来源
                template: `src/${pathStr[2]}/public/index.html`,
                // 在 dist/index.html 的输出
                filename: `${pathStr[2]}.html`,
                title: `这是${pathStr[2]}端`,
                chunks: ['chunk-vendors', 'chunk-common', pathStr[2]]
            }
        })
    }
    return entries
}

console.log(createEntry());

module.exports = {
    lintOnSave:false,
    publicPath: "./",
    //多页面配置
    pages: createEntry(),
    devServer: {
        port:9527,
        open:true,
        openPage: "/pc.html#" //默认打开pc路径
    }
}